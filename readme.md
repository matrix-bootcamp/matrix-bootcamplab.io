# webpage for matrix-bootcamp
A website and example usage of the matrix-web-components to publish
the content of public matrix rooms and spaces on webpages.

> We can use this repository as a starter for a tiny website using the
> `<matrix-room/>` and other web-components.

